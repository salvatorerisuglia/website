---
title: "About"
date: 2019-01-08T40:19:38+01:00
draft: false
---
Crediamo nel Software Libero e nella sua filosofia di apertura e trasparenza, lo usiamo per noi e per i nostri clienti. Usare software Open Source garantisce la libertà di scegliere e cambiare senza rischiare di rimanere ingabbiati. Usiamo principalmente GNU/Linux Debian per la sua aderenza ai principi e SUSE per la sua riconosciuta affidabilità in ambito aziendale. Offriamo la nostra professionalità per installazione, configurazione, gestione, aggiornamento, monitoraggio, manutenzione e supporto.

Software Workers srl è SUSE Certified Resellers - Enterprise Linux accredited partner.
SUSE, società europea pioniera nel software open source, mette a punto soluzioni affidabili per le infrastrutture software-defined e il delivery applicativo 
che offrono alle aziende un livello superiore di controllo e flessibilità. Oltre 25 anni di eccellenza nell’engineering, un’assistenza eccezionale e un 
ecosistema di partner senza confronti sono la base dei prodotti e del supporto che aiutano i nostri clienti a gestire la complessità, ridurre i costi e 
implementare in sicurezza servizi mission-critical. Stabiliamo rapporti duraturi che ci permettono di adattarci e fornire quell’innovazione avanzata che 
occorre per poter avere successo — oggi così come domani. Per maggiori informazioni https://www.suse.com/local/italy/